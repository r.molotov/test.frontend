﻿import React, {FC, ReactElement, useRef, useEffect, useState } from 'react';
import { CreateNoteDto, Client, NoteLookupDto } from "../api/api";
import { FormControl } from "react-bootstrap";

const appClient = new Client('https://localhost:5002');

async function createNote(note: CreateNoteDto){
    await appClient.create('1.0', note);
    console.log('note created');
}

const NoteList: FC<{}> = (): ReactElement => {
    let textInput = useRef(null);
    const [notes, setNotes] = useState<NoteLookupDto[] | undefined>(undefined);
    
    async function getNotes(){
        const noteListVm = await appClient.getAll('1.0');
        setNotes(noteListVm.notes);
    }

    useEffect(() => {
        setTimeout(getNotes, 500);
    }, []);

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            const note: CreateNoteDto = {
                title: event.currentTarget.value,
            };
            createNote(note);
            event.currentTarget.value = '';
            setTimeout(getNotes, 500);
        }
    };

    return (
        <div>
            Notes
            <div>
                <FormControl ref={textInput} onKeyPress={handleKeyPress} />
            </div>
            <section>
                {notes?.map((note) => (
                    <div>{note.title}</div>
                ))}
            </section>
        </div>
    );
};
export default NoteList;
﻿import { useHistory } from "react-router-dom";
import { FC, useEffect } from "react";
import { signinRedirectCallback } from './user-service';

const SigninOidc: FC<{}> = () => {
    const  history = useHistory();
    useEffect(() => {
        async function signinAsync() {
            await signinRedirectCallback();
            history.push('/');
        }

        signinAsync();
    }, [history]);
    return <div>Redirecting...</div>
}

export default SigninOidc;